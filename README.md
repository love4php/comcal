## Description
The project includes the following classes:
- Transaction 
- Repository of transactions
- Commission rules
- Exchange convertor
- File management

The Data is imported with csv format by the user , and after that  Data will be parsed with FileReaderAbstract class.
each row is converted to a transaction object.

To start calculating the commission ,it's necessary to use commission management class and define the transaction commission rules.
and based on the criteria the  commission's rules applied on each data.
and finally the history of each transaction is saved in the TransactionRepository class.



## Requirements
PHP >= 8.0 ( extensions : )
 - JSON PHP Extension
 - OpenSSL PHP Extension
 
Composer 2

Git  

## Install
- clone https://gitlab.com/love4php/comcal
- go to the project directory 
- run composer install

## Execute 

$ php artisan commission:calculate {filename}
{filename} : transaction file address should be on the root of the directory.

Example :
$ php artisan commission:calculate pay.csv


## Testing 
test file address : test\Unit\CommissionTest.php
run $ php artisan test
 consider 5 test : 
- Exchange Test
- Round up Test
- Repository limitation by amount Test
- Repository limitation by count Test
- Commission  Test



