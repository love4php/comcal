<?php


namespace Tests\Unit;

use App\Classes\Transaction\Commission;
use App\Classes\Transaction\Rule\BusinessClientsRule;
use App\Classes\Transaction\Rule\DepositRule;
use App\Classes\Transaction\Rule\PrivateClientsRule;
use App\Classes\Transaction\Transaction;
use App\Classes\Transaction\TransactionFile;
use App\Classes\Transaction\TransactionRepository;
use App\Classes\Utility\Exchanger;
use App\Classes\Utility\Math;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class CommissionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExchange()
    {
        $exchange = Exchanger::getInstance('currency-exchange-rates.json');
        $this->assertEquals($exchange->exchange(1 , 'EUR', 'AED'), 4.147043 );
    }

    public function testRoundUp(){
        $this->assertSame([
            Math::roundUp(0.12345, 2),
            Math::roundUp(0.12045, 2),
            Math::roundUp(0.12945, 2),
            Math::roundUp(0.12, 2),
        ],
        [
            0.13,
            0.13,
            0.13,
            0.12,
        ]);
    }


    public function testRepositoryLimitationByAmount(){
        $repo = new TransactionRepository();
        $limited  = $this->_transactions()->slice(0, 3);
        foreach ($limited as $tr){

            $repo->addTransaction(new Transaction(
                date : $tr[0],
                userId: $tr[1],
                userType: $tr[2],
                operationType: $tr[3],
                amount: $tr[4],
                currency: $tr[5],
            ));

        }

        $last = $limited->last();
        $targetTransaction = new Transaction(
            date : $last[0],
            userId: $last[1],
            userType: $last[2],
            operationType: $last[3],
            amount: $last[4],
            currency: $last[5],
        );

        $this->assertFalse( $repo->getTotal($targetTransaction) > PrivateClientsRule::$MAXIMUM_WITHDRAW);

    }

    public function testRepositoryLimitationByCount(){
        $repo = new TransactionRepository();
        $limited  = $this->_transactions()->slice(0, 3);
        foreach ($limited as $tr){

            $repo->addTransaction(new Transaction(
                date : $tr[0],
                userId: $tr[1],
                userType: $tr[2],
                operationType: $tr[3],
                amount: $tr[4],
                currency: $tr[5],
            ));

        }

        $last = $limited->last();
        $targetTransaction = new Transaction(
            date : $last[0],
            userId: $last[1],
            userType: $last[2],
            operationType: $last[3],
            amount: $last[4],
            currency: $last[5],
        );

        $this->assertTrue( $repo->getCount($targetTransaction) < PrivateClientsRule::$MAXIMUM_OPERATION);
    }

    public function testCommission(){
        $reportFile = new TransactionFile('pay.csv');
        $commissionController = new Commission(
            new Collection([
                DepositRule::class,
                BusinessClientsRule::class,
                PrivateClientsRule::class
            ]),
            new TransactionRepository()
        );

        $results = [];
        foreach($reportFile->getTransactions() as  $transaction){
            $results[]= $commissionController->calculate($transaction);
        }
        $this->assertSame(
            $this->_results(),
            $results
        );

    }

    private function _transactions(): Collection
    {
        return  new Collection([
            ['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'EUR'],
            ['2015-01-01', '4', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-01-05', '4', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-01-05', '1', 'private', 'deposit', '200.00', 'EUR'],
            ['2016-01-06', '2', 'business', 'withdraw', '300.00', 'EUR'],
            ['2016-01-06', '1', 'private', 'withdraw', '30000', 'JPY'],
            ['2016-01-07', '1', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-01-07', '1', 'private', 'withdraw', '100.00', 'USD'],
            ['2016-01-10', '1', 'private', 'withdraw', '100.00', 'EUR'],
            ['2016-01-10', '2', 'business', 'deposit', '10000.00', 'EUR'],
            ['2016-01-10', '3', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-02-15', '1', 'private', 'withdraw', '300.00', 'EUR'],
            ['2016-02-19', '5', 'private', 'withdraw', '3000000', 'JPY'],
        ]);
    }

    private function _results(){
        return [
            0.6,
            3.0,
            0.0,
            0.06,
            1.5,
            0.0,
            0.7,
            0.3,
            0.3,
            3.0,
            0.0,
            0.0,
            8611.42,
        ];
    }
}
