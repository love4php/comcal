<?php

namespace App\Console\Commands;

use App\Classes\Rate;
use App\Classes\Transaction\Commission;
use App\Classes\Transaction\Rule\BusinessClientsRule;
use App\Classes\Transaction\Rule\DepositRule;
use App\Classes\Transaction\Rule\PrivateClientsRule;
use App\Classes\Transaction\TransactionFile;
use App\Classes\Transaction\TransactionRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class Comcal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commission:calculate {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'calculating transaction commissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $filename = $this->argument('filename');
        if(!file_exists($filename)){
            $this->warn('file "'.$filename.'" not exists');
            return 0;
        }

        $reportFile = new TransactionFile($filename);
        $commissionController = new Commission(
            new Collection([
                DepositRule::class,
                BusinessClientsRule::class,
                PrivateClientsRule::class
            ]),
            new TransactionRepository()
        );

        foreach($reportFile->getTransactions() as  $transaction){
            echo $commissionController->calculate($transaction)."\n";
        }

    }


}
