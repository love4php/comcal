<?php
namespace App\Classes\Utility;

use Exception;
use function App\Classes\str_starts_with;

/**
 * @method float _toEUR(string $currency, float $amount);
 */
class Exchanger{

    private static ?Exchanger $instance = null;

    private static mixed $_rate;

    private function __construct(string $reference = null)
    {
        if($reference !== null){
            $content = file_get_contents($reference);
            self::$_rate = json_decode($content, true);
        }else{
            $content = file_get_contents('currency-exchange-rates.json');
            self::$_rate = json_decode($content, true);
            /*
            $response = Http::get('https://developers.paysera.com/tasks/api/currency-exchange-rates');
            if ($response->failed()) {
                throw new \Exception('please check currencty exchange rates url');
            }
            */
        }

    }

    public static function getInstance(?string $reference = null): Exchanger
    {
      if (self::$instance == null)
      {
        self::$instance = new self($reference);
      }
      return self::$instance;
    }

    public function __call($name, $arguments)
    {
        if(str_starts_with($name, '_to') && strlen($name) == 6){
            $convertTo = substr($name, 3);
            return $this->exchange($arguments[1], $arguments[0], $convertTo);
        }
    }

    public function getBase(){
        return self::$_rate['base'];
    }


    public function exchange($amount, $from , $to ){
        if($from == $to){
            return $amount;
        }elseif(self::$_rate['base'] == $from){
            if(!isset(self::$_rate['rates'][$to])){
                throw new Exception(self::$_rate['rates'][$to].'Currency not exists');
            }
            return ($amount * self::$_rate['rates'][$to]);
        }elseif(self::$_rate['base'] == $to){
            if(!isset(self::$_rate['rates'][$from])){
                throw new Exception(self::$_rate['rates'][$from].'Currency not exists');
            }
            return ($amount / self::$_rate['rates'][$from]);
        }
        $step1 = $this->exchange($amount, $from , self::$_rate['base']);
        return $this->exchange( $step1, self::$_rate['base'] , $to);
    }

}
