<?php

namespace App\Classes\Utility;

class Math
{
    /**
     * rounded up
     * @param float $value
     * @param int $precision
     * @return float|int
     */
    public static function roundUp(float $value, int $precision ): float|int
    {
        $pow = pow ( 10, $precision );
        return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow;

    }
}
