<?php

namespace App\Classes\Transaction;

use App\Classes\Transaction\Base\TransactionAbstract;
use Illuminate\Support\Collection;

class Commission
{
    public function __construct(
        protected Collection $_rules,
        protected TransactionRepository $_repository
    )
    {

    }

    /**
     * @param TransactionAbstract $transaction
     * @return int|mixed
     * @throws \Exception
     */
    public function calculate(TransactionAbstract $transaction) : float
    {
        $commission = 0;
        foreach ($this->_rules as $rule){
            $_rule = new $rule($transaction, $this->_repository);
            $result = $_rule->calculate();
            if($result !== false){
                $commission = $result;
                break;
            }
        }
        $this->_repository->addTransaction($transaction);
        return  $commission;

    }
}
