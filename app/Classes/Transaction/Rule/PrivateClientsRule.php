<?php

namespace App\Classes\Transaction\Rule;

class PrivateClientsRule extends RuleAbstract
{
    public static float $COMMISSION_CHARGE = 0.3;
    public static float $MAXIMUM_WITHDRAW = 1000.00;
    public static int $MAXIMUM_OPERATION  = 3;


    /**
     * @return float|bool
     */
    protected function _calculate(): float|bool
    {
        if($this->transaction->getOperationType() != $this->transaction::OPERATION_TYPE_WITHDRAW){
            return false;
        }

        $_amount = $this->transaction->getAmountAsBase();

        $_count = $this->reportRepository->getCount($this->transaction);
        $_total = $this->reportRepository->getTotal($this->transaction);
        if ($_count >= self::$MAXIMUM_OPERATION) {
            $commissionAmount = $_amount;
        } else {
            if ($_total < self::$MAXIMUM_WITHDRAW) {
                $commissionAmount = max(0, $_amount - (self::$MAXIMUM_WITHDRAW - $_total));
            } else {
                $commissionAmount = $_amount;
            }
        }


        return ($commissionAmount * self::$COMMISSION_CHARGE) / 100;
    }
}
