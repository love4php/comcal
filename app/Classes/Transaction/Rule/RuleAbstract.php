<?php

namespace App\Classes\Transaction\Rule;

use App\Classes\Transaction\Transaction;
use App\Classes\Transaction\TransactionRepository;
use App\Classes\Utility\Exchanger;
use App\Classes\Utility\Math;

abstract class RuleAbstract
{

    public function __construct(
        protected Transaction            $transaction,
        protected TransactionRepository  $reportRepository,
    ) {}

    /**
     *
     * @return float|bool
     * @throws \Exception
     */
    final public function calculate(): float|bool
    {
        $commissionPercent = $this->_calculate();

        if($commissionPercent == false){
            return false;
        }

        $exchanger = (Exchanger::getInstance());
        $commissionPercent = $exchanger->exchange(
            $commissionPercent,
            $exchanger->getBase(),
            $this->transaction->getCurrency());
        return Math::roundUp($commissionPercent, 2);
    }

    abstract protected function _calculate(): float|bool;
}
