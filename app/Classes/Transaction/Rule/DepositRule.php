<?php

namespace App\Classes\Transaction\Rule;

class DepositRule extends RuleAbstract
{
    public static float $COMMISSION_CHARGE = 0.03;

    /**
     * @return float|bool
     */
    protected function _calculate(): float|bool
    {
        if($this->transaction->getOperationType() == $this->transaction::OPERATION_TYPE_DEPOSIT){
            return  ($this->transaction->getAmountAsBase() * self::$COMMISSION_CHARGE) / 100;
        }
        return false;
    }
}
