<?php

namespace App\Classes\Transaction\Rule;

class BusinessClientsRule extends RuleAbstract
{
    public static float $COMMISSION_CHARGE = 0.5;
    const USER_TYPE_BUSINESS = 'business';

    /**
     * @return float|bool
     */
    protected function _calculate(): float|bool
    {
        if($this->transaction->getOperationType() != $this->transaction::OPERATION_TYPE_WITHDRAW){
            return false;
        }

        if($this->transaction->getUserType() == self::USER_TYPE_BUSINESS){
            return  ($this->transaction->getAmountAsBase() * self::$COMMISSION_CHARGE) / 100;
        }
        return false;
    }
}
