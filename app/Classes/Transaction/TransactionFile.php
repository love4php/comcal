<?php
namespace App\Classes\Transaction;

use App\Classes\Transaction\Base\FileReaderAbstract;
use Illuminate\Support\Collection;
use function collect;

class TransactionFile extends FileReaderAbstract
{

    protected ?Collection $_transactions = null;

    public function __construct(
        private string $_filename
    ) {
    }

    /**
     * @return Collection
     */
    public function getTransactions() : Collection
    {
        if($this->_transactions == null){
            $file = fopen($this->_filename, 'r');
            $this->_transactions = collect();
            while (($line = fgetcsv($file)) !== FALSE) {
                list($_date, $_userId, $_userType, $_operationType, $_amount, $_currency) = $line;
                $this->_transactions->push(new Transaction($_date, $_userId, $_userType, $_operationType, $_amount, $_currency));
            }
            fclose($file);
        }
        return $this->_transactions;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return true;
    }

}
