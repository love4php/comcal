<?php
namespace App\Classes\Transaction;

use Illuminate\Support\Collection;

class TransactionRepository{

    private Collection $_collection ;

    public function __construct()
    {
        $this->_collection = new Collection();
    }

    /**
     * @param Transaction $transaction
     * @return $this
     * @throws \Exception
     */
    public function addTransaction(Transaction $transaction): static
    {

        $this->_collection->push([
            'date' => $transaction->getDate(),
            'userId' => $transaction->getUserId(),
            'periodId' => $transaction->getPeriodId(),
            'operationType' => $transaction->getOperationType(),
            'currency' => $transaction->getCurrency(),
            'userType' => $transaction->getUserType(),
            'amount' =>  $transaction->getAmount(),
            'amount_base' =>  $transaction->getAmountAsBase(),
        ]);


        return $this;

    }


    /**
     * @param Transaction $transaction
     * @return Collection
     */
    private function _getFiltered(Transaction $transaction): Collection
    {
        return  $this->_collection->filter(function ($repo) use($transaction) {
            if( $repo['userId'] == $transaction->getUserId() &&
                $repo['periodId'] == $transaction->getPeriodId() &&
                $repo['operationType'] == Transaction::OPERATION_TYPE_WITHDRAW
            ){
                return true;
            }
            return false;
        });
    }

    /**
     * @param Transaction $transaction
     * @return float
     */
    public function getTotal(Transaction $transaction) : float {
        $filtered = $this->_getFiltered($transaction);
        return $filtered->sum(function ($repo) {
            return $repo['amount_base'];
        });
    }

    /**
     * @param Transaction $transaction
     * @return int
     */
    public function getCount(Transaction $transaction): int
    {

        return $this->_getFiltered( $transaction)->count();
    }


}
