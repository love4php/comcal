<?php
namespace App\Classes\Transaction\Base;

use Illuminate\Support\Collection;

abstract class FileReaderAbstract{
    abstract public function getTransactions():Collection;
    abstract public function isValid():bool;

}
