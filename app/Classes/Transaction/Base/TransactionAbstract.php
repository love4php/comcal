<?php

namespace App\Classes\Transaction\Base;

use App\Classes\Report\Abstract\getAmountAsEUR;
use App\Classes\Utility\Exchanger;
use Exception;
use function App\Classes\Report\Abstract\str_starts_with;

/**
 * @method getAmountAsEUR float
 */
abstract class TransactionAbstract
{


    public function __construct(
        protected string $date,
        protected int    $userId,
        protected string $userType,
        protected string $operationType,
        protected float  $amount,
        protected string $currency
    )
    {
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getUserType(): string
    {
        return $this->userType;
    }

    /**
     * @return string
     */
    public function getOperationType(): string
    {
        return $this->operationType;
    }


    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return strtoupper($this->currency);
    }

    /**
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        if (str_starts_with($name, 'getAmountAs') && (strlen($name) == 14)) {
            $convertTo = substr($name, 11);
            return (Exchanger::getInstance())->exchange(
                $this->getAmount(),
                $this->getCurrency(),
                $convertTo);
        }

        throw new Exception("method {$name} not exist ");
    }

    /**
     * @return float
     * @throws Exception
     */
    public function getAmountAsBase(): float
    {
        $amount = $this->getAmount();
        $base = (Exchanger::getInstance())->getBase();
        $current = $this->getCurrency();
        if ($base != $current) {
            return (Exchanger::getInstance())
                ->exchange($this->getAmount(), $current, $base);
        }
        return $amount;
    }


    /**
     * @return string
     */
    abstract function getPeriodId(): string;

}
