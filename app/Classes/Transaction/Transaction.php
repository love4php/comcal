<?php

namespace App\Classes\Transaction;

use App\Classes\Transaction\Base\TransactionAbstract;
use App\Classes\Transaction\Rule\RuleAbstract;

class Transaction extends TransactionAbstract
{

    const OPERATION_TYPE_DEPOSIT = 'deposit';
    const OPERATION_TYPE_WITHDRAW = 'withdraw';

    protected array $_rules = [];

    /**
     * @param array|RuleAbstract $rules
     * @return void
     */
    public function setRules(array|RuleAbstract $rules){
        if(!is_array($rules)){
            $rules=[$rules];
        }
        $this->_rules = $rules;
    }


    /**
     * Create priod Id from week and year
     * @throws \Exception
     */
    public function getPeriodId() : string
    {
        $date = new \DateTime($this->getDate());
        $weekId = $date->format("YW");
        if ($date->format("m") == 12 && $date->format("W")) {
            $weekId += 100;
        }
        return (string) $weekId;
    }


}
